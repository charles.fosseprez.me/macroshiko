////////////////////////////////////////////////////////////////////////////////
// Data System
////////////////////////////////////////////////////////////////////////////////
print("ImageJ version:", call("ij.IJ.getVersion")); // same as getVersion()
print("java.version:", call("java.lang.System.getProperty", "java.version"));
print("user.home:", call("java.lang.System.getProperty", "user.home"))
print("--------------------------------------------------------------------------")
print("Time:", call("java.lang.System.currentTimeMillis")); // returns a string
print("Free memory:", call("ij.IJ.freeMemory")); // returns a string
print("Armement des tobogans: ON"); // returns bullshit string	
print("Max memory:", parseInt(call("ij.IJ.maxMemory"))/(1024*1024)+"MB");
////////////////////////////////////////////////////////////////////////////////




//Batch mode supress vision of process, win time X10

batch_opt=true;
setBatchMode(batch_opt);
print("---");
print("setBatchMode is:",batch_opt);
print("---");




//Macro template to process multiple images in a folder


print("Please select the folder containing your images. And only your images");
input = getDirectory("Input directory");
mother = File.getParent(input);

print("Output will be located in result folder at the same level than your image folder");
result=mother+"/result"
File.makeDirectory(result); 

print("Input image shall be of .tiff");
suffix = ".tiff";


//Process all images in input folder
processFolder(input,result);



function processFolder(input,result) {
	list0 = getFileList(input);
	input1=input+"/";
	result1=result+"/";
		
	for (i = 0; i < list0.length; i++) {
		print("================================");
		print(i);		
		name_im=list0[i];
		print(name_im);
		print("================================");
		print("open image in RAM");
		open(input1 + name_im);
		print("convert image to 8-bit");
		run("8-bit");
		print("processing image");

		print("save output image");
		saveAs("PNG", result1 + name_im);
		print("close image");
		close();
	}
}



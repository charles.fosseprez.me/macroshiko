***macroshiko***  
*"Ave Imperator, morituri te salutant."*  
    
**Author:** Charles Fosseprez  
  
*For Shrikanth Tirumani. Great friend, amazing scientist, terrible house keeper.*
   
# Info
  
## Basics
  
Here is some trial to automatically analyse western blot gel with Fiji (ImageJ).

:cactus: This repository is a first draft.
  
A Fiji macro was written to do so. To execute this macro, various way are possible.
[Intro to macro programming](https://imagej.net/Introduction_into_Macro_Programming)


## Pictures
  
The pictures of gel were aquired as Srikanth knows... [add details plz]
  


:bomb: Gels cannot be stacked vertically on the image. Only one gel per image (you can crop from stacked gel image).    
  
![Alt text](./image_README/notgoodstack.png?raw=true "not good image stacked")

:bomb: The image to be processed should only contain the gel (no black background out of the gel).  
Here is an example of image to avoid (need to be cropped before utilization):  

![Alt text](./image_README/notgood.png?raw=true "not good image")
  


:thumbsup: Here is an example of proper image to be processed:  

![Alt text](./image_README/good.png?raw=true "good to go image")
  

  

# Utilization
  
## Normal use
Various steps are required for this macro to work:  
  
  

```mermaid
graph TD;
  A[Prepare image to standard]-->B[Put images in folder];
  B-->C[Launch the macro via Fiji];
```
  
## Development
If one intend to tinker the macro, the change of the value  
```java
DEBUG=true; 
```
to  
```java
DEBUG=false;
```
allow to trigger the DEBUG mode that will display all the crap you need!
  


# Algorithmic
  
  
Here is a little description of the algorythmic path that the macro will follow
  
```mermaid
graph TD
  subgraph "Main Processing"
  A[Open image]-->B
  B[Convert to 8-bit]-->C
  C[Band extraction]-->D
  D[Band analysis]-->E 
  E[Save result] 

  C --> PA
  subgraph "Band extraction"
  PA(Enhance Local Contrast) -->PB
  PB(Median filter) -->PC
  PC(Subtract Background) -->PD
  PD(Scale Y to 10) -->PE 
  PE(Make Binary) -->PF
  PF(Find Maxima) -->PG
  PG(Crop rectangles arround maxima on original image)
  end
  PG --> C


end
```
macro "macroshiko" {



////////////////////////////////////////////////////////////////////////////////
// Data System
////////////////////////////////////////////////////////////////////////////////
print("***");
print("ImageJ version:", call("ij.IJ.getVersion")); // same as getVersion()
print("java.version:", call("java.lang.System.getProperty", "java.version"));
print("user.home:", call("java.lang.System.getProperty", "user.home"))
print("--------------------------------------------------------------------------")
print("Time:", call("java.lang.System.currentTimeMillis")); // returns a string
print("Free memory:", call("ij.IJ.freeMemory")); // returns a string
print("Armement des tobogans: ON"); // returns bullshit string  
print("Max memory:", parseInt(call("ij.IJ.maxMemory"))/(1024*1024)+"MB");
print("***");
////////////////////////////////////////////////////////////////////////////////



DEBUG=true;
print("!!!!!");
print("to change environment, turn DEBUG (line 15) to false/true");
print("path for DEBUG can be modifyed line 29?");
print("!!!!!");

if(DEBUG==true){
print("***");
print("Environement is: DEBUG");
print("***");
batch_opt=false;
//Path separator should be /
input = "C:/Users/charli/Desktop/macroshiko/images_original";
print("Path set to DEBUG: ",input);

} else {
print("***");
print("Environement is: SHIKO");
print("***");
batch_opt=true;
print("Please select the folder containing your images. And only your images");
input = getDirectory("Input directory");
print("Path selected: ",input);
}


//Batch mode supress vision of process, win time X10
setBatchMode(batch_opt);
print("---");
print("setBatchMode is:",batch_opt);
print("---");




//Macro template to process multiple images in a folder



mother = File.getParent(input);

print("Output will be located in result folder at the same level than your image folder");
result=mother+"/result";
File.makeDirectory(result); 

print("Input image shall be of .tiff");
suffix = ".tiff";


//Process all images in input folder
processFolder(input,result,DEBUG);



function processFolder(input,result,DEBUG) {
    list0 = getFileList(input);
    input1=input+"/";
    result1=result+"/";
        
    for (i = 0; i < list0.length; i++) {
        print("================================");
        print(i);       
        name_im=list0[i];
        print(name_im);
        print("================================");
        print("open image in RAM");
        open(input1 + name_im);
        print("convert image to 8-bit");
        run("8-bit");
        print("processing image");
        processIMAGE1(DEBUG);
        print("save output image");
        saveAs("PNG", result1 + name_im);
        print("close image");
        if(DEBUG==false){
            close();
        }
        
    }
}


function processIMAGE0(DEBUG) {
    width = getWidth;
    height = getHeight;
    makeRectangle(0, 0, width, height);
    profile = getProfile();
    Plot.create("Profile","X","Value",profile);
    Plot.show;
    Array.findMaxima(profile, 10, 1);
    if(DEBUG==true){
        waitForUser("This is a DEBUG pause. Press OK if you dare!");
    }
}

function processIMAGE1(DEBUG) {
    width = getWidth;
    height = getHeight;
    rename("ori");
    run("Duplicate...", "title=modif");
    run("Enhance Local Contrast (CLAHE)", "blocksize=127 histogram=256 maximum=3 mask=*None*");
    run("Median...", "radius=10");
    run("Subtract Background...", "rolling=50");
    run("Scale...", "x=1 y=0.01 width="+width+" interpolation=Bilinear average");
    setOption("BlackBackground", false);
    run("Make Binary");
    run("Find Maxima...", "prominence=10 exclude light output=[Point Selection]");

    getSelectionCoordinates(xpoints, ypoints);

    if(DEBUG==true){
        for (j = 0; j < xpoints.length; j++) {
        selectWindow("ori");
        run("Duplicate...", "title=tocrop"+j);
        x = xpoints[j];
        y = ypoints[j];
        print("Got coordinates ("+x+","+y+")");
        makeRectangle(x - 37.5, y - (height/2), 75, height);
        //run("Draw", "slice");
        run("Crop");
        rename("Cropped"+j);
        //run("Duplicate...", "title="+j);
        //saveAs("PNG", result1);
        }
    }

    DEBUGHARD=false;

    if(DEBUGHARD==true){
        for (j = 0; j < xpoints.length; i++) {
        x = xpoints[j];
        y = ypoints[j];
        showMessage("Got coordinates ("+x+","+y+")");
        }
        waitForUser("This is a DEBUG pause. Press OK if you dare!");
    }
}


}








